Rails.application.routes.draw do
  root "pages#home"
  get "about", to: "pages#about"
  resources :artikels
  get "signup", to: "users#new"
  resources :users, except: [:new]
  #post "users", to: "users#create"
end