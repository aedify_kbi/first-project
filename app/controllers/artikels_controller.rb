class ArtikelsController < ApplicationController

  before_action :set_artikel, only: [:show, :edit, :update, :destroy]

  def show
  end

  def index
    @artikels = Artikel.all
  end

  def new
    @artikel = Artikel.new 
  end

  def create
    @artikel = Artikel.new(artikel_params)
    @artikel.user = User.first
    if @artikel.save
      flash[:notice] = "Artikel was created successfully!"
      redirect_to @artikel
   else
    render "new"
   end
  end

  def edit
  end

  def update
    if @artikel.update(artikel_params)
      flash[:notice] = "Artikel was updated successfully!"
      redirect_to @artikel
    else 
      render "edit"
    end
  end

  def destroy
    @artikel.destroy
    redirect_to artikels_path
  end

  private

  def set_artikel
    @artikel = Artikel.find(params[:id])
  end

  def artikel_params
    params.require(:artikel).permit(:title, :description)
  end
 
end