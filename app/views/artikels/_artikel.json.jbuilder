json.extract! artikel, :id, :title, :description, :created_at, :updated_at
json.url artikel_url(artikel, format: :json)
